//
//  Stock-Maximize.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <vector>

using namespace std;

int main(){
    int T;
    cin >> T; // number test tests
    while (T--) {
        long profit = 0;
        long days;
        cin >> days;
         
        vector<long> prices;
        prices.reserve(days);
         
        for (long i=0; i < days; i++) {
            long price;
            cin >> price;
            prices.push_back(price);
        }
        

        long priceMax = prices.at(days-1);
        // profit per day
        for (long i=days-1; i>=0; i--) {
            if (priceMax < prices.at(i)) {
                priceMax =  prices.at(i);
            }else{
                profit += priceMax-prices.at(i);
            }
        }
        cout << profit << endl;
    }
}
