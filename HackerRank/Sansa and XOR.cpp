//
//  Sansa and XOR.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;



int main(){
    int T;
    cin >> T;
    
    while (T--) {
        long result = 0;
        long length;
        cin >> length;
        
        vector<long> data;
        data.reserve(length);
        for (int i = 0 ; i < length; i++) {
            long n;
            cin >> n;
            data.push_back(n);
        }
        
        long window = length;
        long offset = 0;
        while (window>0) {
            long innerResult = 0;
            for (long i=offset; i < window+offset; i++) {
                innerResult = innerResult ^ data.at(i);
                cout << data.at(i);
            }
            cout << endl;
            if (window+offset == length) {
                --window;
                offset = 0;
                cout << "window--" << endl;
            }else{
                ++offset;
                cout << "offset++" << endl;
            }
            result = result ^ innerResult;
            cout << "XOR" << endl;
        }
        
        cout << result << endl;
    }
}
