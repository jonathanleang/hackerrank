//
//  Service-Lane.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <vector>

using namespace std;

int findMin(vector<int> width, int from, int to){
    if (width.size() == 0) {
        return 0;
    }
    int min = width.at(from);
    
    for (int i=from; i<=to; i++) {
        if (min > width.at(i)) {
            min = width.at(i);
        }
    }
    
    return min;
}

int main(){
    int N; // Length of the freeway
    int T; // Number of test cases
    cin >> N;
    cin >> T;
    
    vector<int> width;
    for (int i=0; i<N; i++) {
        int w;
        cin >> w;
        width.push_back(w);
    }
    
    while (T--) {
        int from;
        cin >> from;
        
        int to;
        cin >> to;
        
        cout << findMin(width, from, to) << endl;
    }
    
    
}