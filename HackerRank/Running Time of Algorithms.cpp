//
//  Running Time of Algorithms.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;



int main(){
    int T;
    cin >> T;
    
    vector<int> result;
    
    int number[T];
    
    for(int i=0;i<T;i++){
        cin >> number[i];
    }
    
    int i, j ,tmp, count=0;
    for (i = 1; i < T; i++) {
        j = i;
        while (j > 0 && number[j - 1] > number[j]) {
            tmp = number[j];
            number[j] = number[j - 1];
            number[j - 1] = tmp;
            j--;
            ++count;
        }
    }
    cout << count;
}
