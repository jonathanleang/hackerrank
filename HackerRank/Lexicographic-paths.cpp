//
//  Lexicographic paths.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//



#include<iostream>
#include<algorithm>
#include<vector>
#include <algorithm>
#include <cstdio>

using namespace std;

int main(){
    int T;
    cin >> T;
    
    while (T--) {
        long N, M, K;
        cin >> N;
        cin >> M;
        cin >> K;
        
        
        string path = "";
        
        for (int i=0; i<N; i++) {
            path += "H";
        }
        for (int i=0; i<M; i++) {
            path += "V";
        }
        
        long count = 0;
        do
        {
            if (count == K) {
                cout << path << endl;
            }
            ++count;
        }
        while (next_permutation(path.begin(), path.end()));
    }
}
