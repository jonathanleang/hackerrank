//
//  gem-stone.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/08/2014.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <vector>

using namespace std;

string checkMatchingStones(string a,  string b) {
   string result = "";
   for (unsigned long int i=0; i<a.length(); i++) {
       for (unsigned long int j=0; j<b.length(); j++) {
           if (a.at(i) == b.at(j)) {
               bool found = false;
               for (unsigned long int k=0; k<result.length(); k++) {
                   if (b[j] == result.at(k)) {
                       found = true;
                       continue;
                   }
               }
               if (!found)
                   result += b[j];
           }
       }
   }
   return result;
}

int main() {
 unsigned long int totalStones;
 cin >> totalStones;
 string sameStone;
 cin >> sameStone;
 
 for (unsigned long int i=1; i<totalStones; i++) {
   string stone;
   cin >> stone;
   sameStone = checkMatchingStones(sameStone, stone);
 }
 
 cout << sameStone.length();
 return 0;
}
