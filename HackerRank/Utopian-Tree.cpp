//
//  Utopian-Tree.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/08/2014.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

using namespace std;

int height(int n) {
   int height = 1;
   for (int i=0; i<n; i++) {
       if ((i%2) == 0){
           height += height;
       }else{
           height += 1;
       }
   }
   return height;
}
int main() {
   int T;
   cin >> T;
   while (T--) {
       int n;
       cin >> n;
       cout << height(n) << endl;
   }
}
