//
//  A-Game-of-Knights.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 31/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//

#include<iostream>
#include<algorithm>
#include<vector>
#include <algorithm>
#include <cstdio>

using namespace std;


int main(){
    int T;
    cin >> T;
    
    struct knight {
        int x;
        int y;
    };
    
    while (T--) {
        int N, K, X;
        cin >> N; // Total Knights
        cin >> K; // Can move number of Knights
        cin >> X; // Moves
        
        
        knight knights[N];
        for (int i=0; i<N; i++) {
            cin >> knights[i].x >> knights[i].y;
        }
        
        bool player1 = true;
        // take a move
        bool noMoves = false;
        while (!noMoves) {
            int moves = X;
            int numKnights = K;
            
            noMoves = true;
            for (int i=0; i<N; i++) {
                if ((knights[i].x >= 1 && knights[i].y >= 2)
                  ||(knights[i].x >= 2 && knights[i].y >= 1) ) {
                    // can move
                    
                    int canMove = 0;
                    if (knights[i].x > knights[i].y) {
                        canMove = (knights[i].x / 2 );
                        if (canMove > knights[i].y) {
                            canMove = knights[i].y;
                        }
                        if (canMove > moves) {
                            canMove = moves;
                        }
                        knights[i].x -= 2 * canMove;
                        knights[i].y -= 1 * canMove;
                    }else{
                        canMove = (knights[i].y / 2 );
                        if (canMove > knights[i].x) {
                            canMove = knights[i].x;
                        }
                        if (canMove > moves) {
                            canMove = moves;
                        }
                        knights[i].x -= 1 * canMove;
                        knights[i].y -= 2 * canMove;
                    }
                    moves -= canMove;
                    --numKnights;
                    noMoves = false;
                }else{
                    // no move for this one
                    break;
                }
                player1 = !player1;
                if (numKnights == 0) {
                    break;
                }
                if (moves == 0) {
                    break;
                }
            }
        }
        
        if (!player1) {
            cout << "First player wins" << endl;
        }else{
            cout << "Second player wins" << endl;
        }
        
    }
}