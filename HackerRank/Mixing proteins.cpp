//
//  Mixing proteins.cpp
//  HackerRank
//
//  Created by Jonathan Leang on 30/8/14.
//  Copyright (c) 2014 Jonathan Leang. All rights reserved.
//  https://www.hackerrank.com/contests/w9/challenges/pmix


#include<iostream>
#include<algorithm>
#include<vector>
#include <algorithm>
#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(){
    long N,K;
    
    string input = "";
    getline(cin, input);
    stringstream readN(input);
    readN >> N >> K;
    
    
    string ring;
    bool done = false;
    getline(cin, ring);
    
    // mutate
    long count = 0;
    char next;
    bool lastCycle = false;
    while (!done && count < K) {
        bool allA = true;
        ++count;
        lastCycle = false;
        char firstChar = ring[0];
        for (int i=0; i<N; i++) {
            if (i == N-1) {
                next = firstChar;
                lastCycle = true;
            }else{
                next = ring[i+1];
            }
//            cout << i << ": " << ring[i] << next;
            if(ring[i] == 'A'){
                if (next == 'A') {
                }else if (next == 'B') {
                    ring[i] = 'B';
                }else if(next == 'C'){
                    ring[i] = 'C';
                }else if(next == 'D'){
                    ring[i] = 'D';
                }
            }else if(ring[i] == 'B'){
                if (next == 'A') {
                }else if (next == 'B') {
                    ring[i] = 'A';
                }else if(next == 'C'){
                    ring[i] = 'D';
                }else if(next == 'D'){
                    ring[i] = 'C';
                }
            }else if(ring[i] == 'C'){
                if (next == 'A') {
                }else if (next == 'B') {
                    ring[i] = 'D';
                }else if(next == 'C'){
                    ring[i] = 'A';
                }else if(next == 'D'){
                    ring[i] = 'B';
                }
            }else if(ring[i] == 'D'){
                if (next == 'A') {
                }else if (next == 'B') {
                    ring[i] = 'C';
                }else if(next == 'C'){
                    ring[i] = 'B';
                }else if(next == 'D'){
                    ring[i] = 'A';
                }
            }
            if (ring[i] != 'A') {
                allA = false;
            }
//            cout << " => " << ring[i] << endl;
            if (lastCycle) {
                cout << ring << endl;
                break;
            }
        }
        if (allA){
            break;
        }
    }
    cout << ring << endl;
    return 0;
}
